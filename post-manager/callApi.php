<?php 
require '../wp-load.php';
function callAPI($method, $url, $data){
   $curl = curl_init();

   switch ($method){
      case "POST":
         curl_setopt($curl, CURLOPT_POST, 1);
         if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
         break;
      case "PUT":
         curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
         if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
         break;
      default:
         if ($data)
            $url = sprintf("%s?%s", $url, http_build_query($data));
   }

   // OPTIONS:
   curl_setopt($curl, CURLOPT_URL, $url);
   curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Token:9f7ed096303713c6e36ad51570616277',
      'Secret:11c698fafa2e29a71554441570616277',
      'Content-Type: application/json',
   ));
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

   // EXECUTE:
   $result = curl_exec($curl);
   if(!$result){die("Connection Failure");}
   curl_close($curl);
   return $result;
}
function slugify($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}
/**
 * post_exists_by_slug.
 *
 * @return mixed boolean false if no post exists; post ID otherwise.
 */
function post_exists_by_slug( $post_slug ) {
    $args_posts = array(
        'post_type'      => 'post',
        'post_status'    => 'any',
        'name'           => $post_slug,
        'posts_per_page' => 1,
    );
    $loop_posts = new WP_Query( $args_posts );
    if ( ! $loop_posts->have_posts() ) {
        return false;
    } else {
        $loop_posts->the_post();
        return $loop_posts->post->ID;
    }
}
function generate_geatured_image( $image_url, $post_id  ){
    switch_to_blog(1);
    $upload_dir = wp_upload_dir();
    $image_data = file_get_contents($image_url);
    $filename = basename($image_url);
    if(wp_mkdir_p($upload_dir['path']))     $file = $upload_dir['path'] . '/' . $filename;
    else                                    $file = $upload_dir['basedir'] . '/' . $filename;
    file_put_contents($file, $image_data);

    $wp_filetype = wp_check_filetype($filename, null );
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
    $res1= wp_update_attachment_metadata( $attach_id, $attach_data );
    $res2= set_post_thumbnail( $post_id, $attach_id );
}


$urlconsulta = 'http://localhost/feyalegria/post-manager/biblioteca.json';
$get_data = callAPI('GET', $urlconsulta, false);
$response = json_decode($get_data, true);
switch_to_blog(1);
$i = 0;
foreach($response as $post){
  if($i<6){
    $title = $post['titulo'];
    $slug =  slugify($title);
    $content =  $post['descripcion'];
    $autor =  $post['autor'];
    $category = $post['categoria'];
    $type = $post['type'];
    $publisher = $post['publisher'];
    $year = $post['year'];
    $pages = $post['pages'];
    $dest = $post['dest'];
    $language = $post['language'];
    $url= $post['url'];
    if( !post_exists_by_slug( $slug ) ) {
       $post_id = wp_insert_post(
           array(
               'post_author'       =>   2,
               'post_name'         =>   $slug,
               'post_title'        =>   $title,
               'post_content'      =>   $content,
               'post_status'       =>   'publish',
               'post_type'         =>   'post',
               'post_category'     =>   array(81),
               'comment_status'    =>   'closed',
               'ping_status'       =>   'closed',
           )
       );
       $image = $post['imagen'];
       if(!empty($image)){
          $cleaner = substr($image, 0, strpos($image, "?"));
          $test = generate_geatured_image($cleaner,$post_id);
       }
      update_post_meta( $post_id, 'library_author', $autor );
      update_post_meta( $post_id, 'library_category', $category );
      update_post_meta( $post_id, 'library_type', $type );
      update_post_meta( $post_id, 'library_publisher', $publisher );
      update_post_meta( $post_id, 'library_year', $year );
      update_post_meta( $post_id, 'library_pages', $pages );
      update_post_meta( $post_id, 'library_dest', $dest);
      update_post_meta( $post_id, 'library_language', $language );
      update_post_meta( $post_id, 'library_url', $url );
    } else {
       $post_id = "Ya existe el post";
    }
    echo $post_id.'<br />';
  }
  $i++;
}
?>