<?php

$nicdark_themename = "charityfoundation";

//TGMPA required plugin
require_once get_template_directory() . '/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'nicdark_register_required_plugins' );
function nicdark_register_required_plugins() {

    $nicdark_plugins = array(

        //donations
        array(
            'name'      => esc_html__( 'Donations', 'charityfoundation' ),
            'slug'      => 'nd-donations',
            'required'  => true,
        ),

        //cf7
        array(
            'name'      => esc_html__( 'Contact Form 7', 'charityfoundation' ),
            'slug'      => 'contact-form-7',
            'required'  => true,
        ),

        //wp import
        array(
            'name'      => esc_html__( 'Wordpress Importer', 'charityfoundation' ),
            'slug'      => 'wordpress-importer',
            'required'  => true,
        ),


        //nd shortcodes
        array(
            'name'      => esc_html__( 'Shortcodes', 'charityfoundation' ),
            'slug'      => 'nd-shortcodes',
            'required'  => true,
        ),


        //revslider
        array(
            'name'               => esc_html__( 'Revolution Slider', 'charityfoundation' ),
            'slug'               => 'revslider', // The plugin slug (typically the folder name).
            'source'             => get_template_directory().'/plugins/revslider.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
        ),
        
        //Visual Composer
        array(
            'name'               => esc_html__( 'Visual Composer', 'charityfoundation' ),
            'slug'               => 'js_composer', // The plugin slug (typically the folder name).
            'source'             => get_template_directory().'/plugins/js_composer.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
        ),


        //WooCommerce
        array(
            'name'               => esc_html__( 'Woo Commerce', 'charityfoundation' ),
            'slug'               => 'woocommerce', // The plugin slug (typically the folder name).
            'source'             => get_template_directory().'/plugins/woocommerce.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
        ),


        //Events Calendar
        array(
            'name'               => esc_html__( 'Events Calendar', 'charityfoundation' ),
            'slug'               => 'the-events-calendar', // The plugin slug (typically the folder name).
            'source'             => get_template_directory().'/plugins/the-events-calendar.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
        ),


    );


    $nicdark_config = array(
        'id'           => 'charityfoundation',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                      // Default absolute path to bundled plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table. 
    );

    tgmpa( $nicdark_plugins, $nicdark_config );
}
//END tgmpa


//translation
load_theme_textdomain( 'charityfoundation', get_template_directory().'/languages' );


//register my menus
function nicdark_register_my_menus() {
  register_nav_menu( 'main-menu', esc_html__( 'Main Menu', 'charityfoundation' ) );
  register_nav_menu( 'footer-menu', esc_html__( 'Footer Menu', 'charityfoundation' ) );
  register_nav_menu( 'footer2-menu', esc_html__( 'Footer Menu 2', 'charityfoundation' ) ); 
}
add_action( 'init', 'nicdark_register_my_menus' );


//Content_width
if (!isset($content_width )) $content_width  = 1180;


//automatic-feed-links
add_theme_support( 'automatic-feed-links' );

//post-thumbnails
add_theme_support( "post-thumbnails" );

//post-formats
add_theme_support( 'post-formats', array( 'quote', 'image', 'link', 'video', 'gallery', 'audio' ) );

//title tag
add_theme_support( 'title-tag' );

// Sidebar
function nicdark_add_sidebars() {

    // Sidebar Main
    register_sidebar(array(
        'name' =>  esc_html__('Sidebar','charityfoundation'),
        'id' => 'nicdark_sidebar',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
        'name' =>  esc_html__('Footer Info','charityfoundation'),
        'id' => 'nicdark_footerinfo',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
        'name' =>  esc_html__('Site Info','charityfoundation'),
        'id' => 'nicdark_siteinfo',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ));

}
add_action( 'widgets_init', 'nicdark_add_sidebars' );

//add css and js
function nicdark_enqueue_scripts()
{
	
    //css
    wp_enqueue_style( 'nicdark-style', get_stylesheet_uri() );
    wp_enqueue_style( 'nicdark-fonts', nicdark_google_fonts_url(), array(), '1.0.0' );

    //comment-reply
    if ( is_singular() ) wp_enqueue_script( 'comment-reply' );

    wp_enqueue_script('jquery');

}
add_action("wp_enqueue_scripts", "nicdark_enqueue_scripts");
//end js


//logo settings
add_action('customize_register','nicdark_customizer_logo');
function nicdark_customizer_logo( $wp_customize ) {
  

    //Logo
    $wp_customize->add_setting( 'nicdark_customizer_logo_img', array(
      'type' => 'option', // or 'option'
      'capability' => 'edit_theme_options',
      'theme_supports' => '', // Rarely needed.
      'default' => '',
      'transport' => 'refresh', // or postMessage
      'sanitize_callback' => 'nicdark_sanitize_callback_logo_img',
      //'sanitize_js_callback' => '', // Basically to_json.
    ) );
    $wp_customize->add_control( 
        new WP_Customize_Media_Control( 
            $wp_customize, 
            'nicdark_customizer_logo_img', 
            array(
              'label' => esc_html__( 'Logo', 'charityfoundation' ),
              'section' => 'title_tagline',
              'mime_type' => 'image',
            ) 
        ) 
    );

    //sanitize_callback
    function nicdark_sanitize_callback_logo_img($nicdark_logo_img_value) {
        return absint($nicdark_logo_img_value);
    }


}
//end logo settings


//woocommerce support
add_theme_support( 'woocommerce' );

//define nicdark theme option
function nicdark_theme_setup(){
    add_option( 'nicdark_theme_author', 1, '', 'yes' );
    update_option( 'nicdark_theme_author', 1 );
}
add_action( 'after_setup_theme', 'nicdark_theme_setup' );



//START add google fonts
function nicdark_google_fonts_url() {
    $nicdark_font_url = '';
    if ( 'off' !== _x( 'on', 'Google font: on or off', 'charityfoundation' ) ) {
        $nicdark_font_url = add_query_arg( 'family', urlencode( 'Poppins:300,400,700' ), "//fonts.googleapis.com/css" );
    }
    return $nicdark_font_url;
}
//END add google fonts







// creación metadata para video

add_action('add_meta_boxes', 'feyalegria_meta_boxes');
function feyalegria_meta_boxes() {
    add_meta_box( 'feyalegria-meta-box', __('Datos de la entrada'), 'feyalegria_meta_box_callback' );
}

function feyalegria_meta_box_callback( $post ) {
     //nonce. See http://codex.wordpress.org/Function_Reference/wp_nonce_field
     wp_nonce_field( 'feyalegria_meta_box', 'feyalegria_meta_box_noncename' );
    
     //Get the current values of meta fields to pre-populate the custom fields
     $post_meta = get_post_custom($post->ID);

     //The input text
     $current_value = '';
     if( isset( $post_meta['embed'][0] ) ) {
         $current_value = $post_meta['embed'][0];
     }
     ?>
     <p>
         <label class="label" for="embed"><?php _e("Url del Video"); ?></label>
         <input  name="embed" id="embed" type="text" value="<?php echo $current_value; ?>">
     </p>


     <?php
}

add_action('save_post', 'feyalegria_save_custom_fields', 10, 2);
function feyalegria_save_custom_fields($post_id, $post){


         // Segundo, comprobamos el nonce como medida de seguridad
    if ( !isset( $_POST['feyalegria_meta_box_noncename'] ) || ! wp_verify_nonce( $_POST['feyalegria_meta_box_noncename'], 'feyalegria_meta_box' ) ) {
            return;
    }

    // Segundo, si hemos recibido valor de un custom field, los actualizamos
    // El saneado/validación se hace automáticamente en el callback definido en el paso 2
    if( isset( $_POST['embed'] ) && $_POST['embed'] != "" ) {
        update_post_meta( $post_id, 'embed', $_POST['embed'] );
    } else {
        // Opcional
        // $_POST['embed'] no tiene valor establecido,
        // eliminar el meta field de la base de datos si existe previamente
        delete_post_meta( $post_id, 'embed' );
    }

}
add_shortcode( 'feyalegria_map', 'mapfya' );

function mapfya() {
    $html = '<div id="mapsvg"></div>';
    $html .= '<script type="text/javascript">
jQuery(document).ready(function(){
jQuery("#mapsvg").mapSvg({width: 1366,height: 768, mouseOver: function(e, mapsvg) { var idReg = this.id; var imgregion = "#img-"+idReg; jQuery("#"+idReg).css("fill","url("+imgregion+")");},onClick: function(e, mapsvg) { var idReg = this.id; var imgregion = "#img-"+idReg; jQuery("#"+idReg).css("fill","url("+imgregion+")"); jQuery("#mapsvg-menu-regions").hide();}, colors: {baseDefault: "#000000",background: "#ffffff",selected: 30,hover: 10,directory: "#fafafa",status: {},base: "d20a11",disabled: "#ffb1b1"},regions: {td: {id: "td",\'id_no_spaces\': "td",tooltip: "<div class=\'wrap-tooltip\'><h1>República del Chad</h1><p>Fundación: 2007<br />Participantes: 12.012<br />Colaboradores: 278<br />Puntos geográficos: 40</p></div>",popover: "<img src=\'https://d3pugrm67vf9n7.cloudfront.net/mapa/chad.jpg\' class=\'pic_map\' /><div class=\'wrap_content_c_pop\'><h1>República del Chad</h1><p>Fundación: 2007<br />Participantes: 12.012<br />Colaboradores: 278<br />Puntos geográficos: 40</p></div>",data: {countryName: "República del Chad"}},ar: {id: "ar",\'id_no_spaces\': "ar",tooltip: "<div class=\'wrap-tooltip\'><h1>Argentina</h1><p>Fundación: 1996<br />Participantes: 6.418<br />Colaboradores: 490<br />Puntos geográficos: 26</p></div>",popover: "<img src=\'https://d3pugrm67vf9n7.cloudfront.net/mapa/argentina.jpg\' class=\'pic_map\' /><div class=\'wrap_content_c_pop\'><h1>Argentina</h1><p>Fundación: 1996<br />Participantes: 6.418<br />Colaboradores: 490<br />Puntos geográficos: 26</p></div>",data: {countryName: "Argentina"}},bo: {id: "bo",\'id_no_spaces\': "bo",tooltip: "<div class=\'wrap-tooltip\'><h1>Bolivia</h1><p>Fundación: 1966<br />Participantes: 384.802<br />Colaboradores: 10.034<br />Puntos geográficos: 407</p></div>",popover: "<img src=\'https://d3pugrm67vf9n7.cloudfront.net/mapa/bolivia.jpg\' class=\'pic_map\' /><div class=\'wrap_content_c_pop\'><h1>Bolivia</h1><p>Fundación: 1966<br />Participantes: 384.802<br />Colaboradores: 10.034<br />Puntos geográficos: 407</p></div>",data: {countryName: "Bolivia"}},br: {id: "br",\'id_no_spaces\': "br",tooltip: "<div class=\'wrap-tooltip\'><h1>Brasil</h1><p>Fundación: 1981<br />Participantes: 9.812<br />Colaboradores: 543<br />Puntos geográficos: 25</p></div>",popover: "<img src=\'https://d3pugrm67vf9n7.cloudfront.net/mapa/brasil.jpg\' class=\'pic_map\' /><div class=\'wrap_content_c_pop\'><h1>Brasil</h1><p>Fundación: 1981<br />Participantes: 9.812<br />Colaboradores: 543<br />Puntos geográficos: 25</p></div>",data: {countryName:"Brasil"}},cg: {id: "cg",\'id_no_spaces\': "cg",tooltip: "<div class=\'wrap-tooltip\'><h1>República Democrática del Congo</h1><p>Fundación: 2014<br />Participantes: 10.885<br />Colaboradores: 405<br />Puntos geográficos: 6</p></div>",popover: "<img src=\'https://d3pugrm67vf9n7.cloudfront.net/mapa/congo.jpg\' class=\'pic_map\' /><div class=\'wrap_content_c_pop\'><h1>República Democrática del Congo</h1><p>Fundación: 2014<br />Participantes: 10.885<br />Colaboradores: 405<br />Puntos geográficos: 6</p></div>",data: {countryName:"República Democrática del Congo"}},cl: {id: "cl",\'id_no_spaces\': "cl",tooltip: "<div class=\'wrap-tooltip\'><h1>Chile</h1><p>Fundación: 2005<br />Participantes: 3.969<br />Colaboradores: 380<br />Puntos geográficos: 13</p></div>",popover: "<img src=\'https://d3pugrm67vf9n7.cloudfront.net/mapa/chile.jpg\' class=\'pic_map\' /><div class=\'wrap_content_c_pop\'><h1>Chile</h1><p>Fundación: 2005<br />Participantes: 3.969<br />Colaboradores: 380<br />Puntos geográficos: 13</p></div>",data: {countryName:"Chile"}},co: {id: "co",\'id_no_spaces\': "co",tooltip: "<div class=\'wrap-tooltip\'><h1>Colombia</h1><p>Fundación: 1971<br />Participantes: 154.444<br />Colaboradores: 2.966<br />Puntos geográficos: 101</p></div>",popover: "<img src=\'https://d3pugrm67vf9n7.cloudfront.net/mapa/colombia.jpg\' class=\'pic_map\' /><div class=\'wrap_content_c_pop\'><h1>Colombia</h1><p>Fundación: 1971<br />Participantes: 154.444<br />Colaboradores: 2.966<br />Puntos geográficos: 101</p></div>",data: {countryName:"Colombia"}},do: {id: "do",\'id_no_spaces\': "do",tooltip: "<div class=\'wrap-tooltip\'><h1>República Dominicana</h1><p>Fundación: 1990<br />Participantes: 103.267<br />Colaboradores: 2.529<br />Puntos geográficos: 63</p></div>",popover: "<img src=\'https://d3pugrm67vf9n7.cloudfront.net/mapa/rdominicana.jpg\' class=\'pic_map\' /><div class=\'wrap_content_c_pop\'><h1>República Dominicana</h1><p>Fundación: 1990<br />Participantes: 103.267<br />Colaboradores: 2.529<br />Puntos geográficos: 63</p></div>",data: {countryName:"República Dominicana"}},ec: {id: "ec",\'id_no_spaces\': "ec",tooltip: "<div class=\'wrap-tooltip\'><h1>Ecuador</h1><p>Fundación: 1964<br />Participantes: 30.402<br />Colaboradores: 1.589<br />Puntos geográficos: 73</p></div>",popover: "<img src=\'https://d3pugrm67vf9n7.cloudfront.net/mapa/ecuadro.jpg\' class=\'pic_map\' /><div class=\'wrap_content_c_pop\'><h1>Ecuador</h1><p>Fundación: 1964<br />Participantes: 30.402<br />Colaboradores: 1.589<br />Puntos geográficos: 73</p></div>",data: {countryName:"Ecuador"}},es: {id: "es",\'id_no_spaces\': "es",tooltip: "<div class=\'wrap-tooltip\'><h1>España</h1><p>Fundación: 1985<br />Participantes: 4.681<br />Colaboradores: 418<br />Puntos geográficos: 13</p></div>",popover: "<img src=\'https://d3pugrm67vf9n7.cloudfront.net/mapa/espana.jpg\' class=\'pic_map\' /><div class=\'wrap_content_c_pop\'><h1>España</h1><p>Fundación: 1985<br />Participantes: 4.681<br />Colaboradores: 418<br />Puntos geográficos: 13</p></div>",data: {countryName:"España"}},gt: {id: "gt",\'id_no_spaces\': "gt",tooltip: "<div class=\'wrap-tooltip\'><h1>Guatemala</h1><p>Fundación: 1976<br />Participantes: 31.688<br />Colaboradores: 880<br />Puntos geográficos: 51</p></div>",popover: "<img src=\'https://d3pugrm67vf9n7.cloudfront.net/mapa/guatemala.jpg\' class=\'pic_map\' /><div class=\'wrap_content_c_pop\'><h1>Guatemala</h1><p>Fundación: 1976<br />Participantes: 31.688<br />Colaboradores: 880<br />Puntos geográficos: 51</p></div>",data: {countryName:"Guatemala"}},hn: {id: "hn",\'id_no_spaces\': "hn",tooltip: "<div class=\'wrap-tooltip\'><h1>Honduras</h1><p>Fundación: 2000<br />Participantes: 10.165<br />Colaboradores: 318<br />Puntos geográficos: 34</p></div>",popover: "<img src=\'https://d3pugrm67vf9n7.cloudfront.net/mapa/honduras.jpg\' class=\'pic_map\' /><div class=\'wrap_content_c_pop\'><h1>Honduras</h1><p>Fundación: 2000<br />Participantes: 10.165<br />Colaboradores: 318<br />Puntos geográficos: 34</p></div>",data: {countryName:"Honduras"}},ht: {id: "ht",\'id_no_spaces\': "ht",tooltip: "<div class=\'wrap-tooltip\'><h1>Haití</h1><p>Fundación: 2006<br />Participantes: 4.940<br />Colaboradores: 319<br />Puntos geográficos: 19</p></div>",popover: "<img src=\'https://d3pugrm67vf9n7.cloudfront.net/mapa/haiti.jpg\' class=\'pic_map\' /><div class=\'wrap_content_c_pop\'><h1>Haití</h1><p>Fundación: 2006<br />Participantes: 4.940<br />Colaboradores: 319<br />Puntos geográficos: 19</p></div>",data: {countryName:"Haití"}},it: {id: "it",\'id_no_spaces\': "it",tooltip: "<div class=\'wrap-tooltip\'><h1>Italia</h1><p>Fundación: 2001<br />Participantes: 139<br />Colaboradores: 44<br />Puntos geográficos: 3</p></div>",popover: "<img src=\'https://d3pugrm67vf9n7.cloudfront.net/mapa/italia.jpg\' class=\'pic_map\' /><div class=\'wrap_content_c_pop\'><h1>Italia</h1><p>Fundación: 2001<br />Participantes: 139<br />Colaboradores: 44<br />Puntos geográficos: 3</p></div>",data: {countryName:"Italia"}},mg: {id: "mg",\'id_no_spaces\': "mg",tooltip: "<div class=\'wrap-tooltip\'><h1>Madagascar</h1><p>Fundación: 2013<br />Participantes: 4.750<br />Colaboradores: 130<br />Puntos geográficos: 42</p></div>",popover: "<img src=\'https://d3pugrm67vf9n7.cloudfront.net/mapa/madagascar.jpg\' class=\'pic_map\' /><div class=\'wrap_content_c_pop\'><h1>Madagascar</h1><p>Fundación: 2013<br />Participantes: 4.750<br />Colaboradores: 130<br />Puntos geográficos: 42</p></div>",data: {countryName:"Madagascar"}},ni: {id: "ni",\'id_no_spaces\': "ni",tooltip: "<div class=\'wrap-tooltip\'><h1>Nicaragua</h1><p>Fundación: 1974<br />Participantes: 55.578<br />Colaboradores: 854<br />Puntos geográficos: 47</p></div>",popover: "<img src=\'https://d3pugrm67vf9n7.cloudfront.net/mapa/nicaragua.jpg\' class=\'pic_map\' /><div class=\'wrap_content_c_pop\'><h1>Nicaragua</h1><p>Fundación: 1974<br />Participantes: 55.578<br />Colaboradores: 854<br />Puntos geográficos: 47</p></div>",data: {countryName:"Nicaragua"}},pa: {id: "pa",\'id_no_spaces\': "pa",tooltip: "<div class=\'wrap-tooltip\'><h1>Panamá</h1><p>Fundación: 1965<br />Participantes: 2.425<br />Colaboradores: 10<br />Puntos geográficos: 4</p></div>",popover: "<img src=\'https://d3pugrm67vf9n7.cloudfront.net/mapa/panama.jpg\' class=\'pic_map\' /><div class=\'wrap_content_c_pop\'><h1>Panamá</h1><p>Fundación: 1965<br />Participantes: 2.425<br />Colaboradores: 10<br />Puntos geográficos: 4</p></div>",data: {countryName:"Panamá"}},pe: {id: "pe",\'id_no_spaces\': "pe",tooltip: "<div class=\'wrap-tooltip\'><h1>Perú</h1><p>Fundación: 1966<br />Participantes: 132.799<br />Colaboradores: 132.799<br />Puntos geográficos: 81</p></div>",popover: "<img src=\'https://d3pugrm67vf9n7.cloudfront.net/mapa/peru.jpg\' class=\'pic_map\' /><div class=\'wrap_content_c_pop\'><h1>Perú</h1><p>Fundación: 1966<br />Participantes: 132.799<br />Colaboradores: 132.799<br />Puntos geográficos: 81</p></div>",data: {countryName:"Perú"}},py: {id: "py",\'id_no_spaces\': "py",tooltip: "<div class=\'wrap-tooltip\'><h1>Paraguay</h1><p>Fundación: 1992<br />Participantes: 11.795<br />Colaboradores: 669<br />Puntos geográficos: 68</p></div>",popover: "<img src=\'https://d3pugrm67vf9n7.cloudfront.net/mapa/paraguay.jpg\' class=\'pic_map\' /><div class=\'wrap_content_c_pop\'><h1>Paraguay</h1><p>Fundación: 1992<br />Participantes: 11.795<br />Colaboradores: 669<br />Puntos geográficos: 68</p></div>",data: {countryName:"Paraguay"}},sv: {id: "sv",\'id_no_spaces\': "sv",tooltip: "<div class=\'wrap-tooltip\'><h1>El Salvador</h1><p>Fundación: 1969<br />Participantes: 20.335<br />Colaboradores: 482<br />Puntos geográficos: 22</p></div>",popover: "<img src=\'https://d3pugrm67vf9n7.cloudfront.net/mapa/elsalvador.jpg\' class=\'pic_map\' /><div class=\'wrap_content_c_pop\'><h1>El Salvador</h1><p>Fundación: 1969<br />Participantes: 20.335<br />Colaboradores: 482<br />Puntos geográficos: 22</p></div>",data: {countryName:"El Salvador"}},us: {id: "us",\'id_no_spaces\': "us",disabled: true,data: {},fill: "rgba(178,178,178)"},uy: {id: "uy",\'id_no_spaces\': "uy",tooltip: "<div class=\'wrap-tooltip\'><h1>Uruguay</h1><p>Fundación: 2008<br />Participantes: 2.033<br />Colaboradores: 214<br />Puntos geográficos: 27</p></div>",popover: "<img src=\'https://d3pugrm67vf9n7.cloudfront.net/mapa/uruguay.jpg\' class=\'pic_map\' /><div class=\'wrap_content_c_pop\'><h1>Uruguay</h1><p>Fundación: 2008<br />Participantes: 2.033<br />Colaboradores: 214<br />Puntos geográficos: 27</p></div>",data: {countryName: "Uruguay"}},ve: {id: "ve",\'id_no_spaces\': "ve",tooltip: "<div class=\'wrap-tooltip\'><h1>Venezuela</h1><p>Fundación: 1955<br />Participantes: 341.196<br />Colaboradores: 11.927<br />Puntos geográficos: 448</p></div>",popover: "<img src=\'https://d3pugrm67vf9n7.cloudfront.net/mapa/venezuela.jpg\' class=\'pic_map\' /><div class=\'wrap_content_c_pop\'><h1>Venezuela</h1><p>Fundación: 1955<br />Participantes: 341.196<br />Colaboradores: 11.927<br />Puntos geográficos: 448</p></div>",data: {countryName:"Venezuela"}}},viewBox: [20,240,956,537],cursor: "pointer",tooltips: {mode: "title",on: false,priority: "local",position: "bottom-right"},popovers: {mode: "off",on: false,priority: "local",position: "top",centerOn: false,width: 500,maxWidth: 50,maxHeight: 50,resetViewboxOnClose: true,mobileFullscreen: true},gauge: {on: false,labels: {low: "low",high: "high"},colors: {lowRGB: {r: 85,g: 0,b: 0,a: 1},highRGB: {r: 238,g: 0,b: 0,a: 1},low: "#550000",high: "#ee0000",diffRGB: {r: 153,g: 0,b: 0,a: 0}},min: 0,max: false},menu: {on: true,containerId: "mapsvg-menu-regions",template: function(region){ 
    return \'<li id="slc-\' + region.id + \'"><a href="#\' + region.id + \'">\' + (region.data.countryName||region.id)+ \'</a></li>\'}},source: "'.get_template_directory_uri().'/assets/img/mapFeAlegria.svg",title: "MapFeAlegria",responsive: true});
});
</script>';
    return $html;  
}
add_shortcode( 'feyalegria_map_select', 'selectmap' );
function selectmap($atts){
    $type = $atts['type'];
    if($type == 'country' ){
        $html = '<div class="select-map"><a class="call-to" data-menu="mapsvg-menu-regions">Países donde estamos </a><i class="fa fa-sort-down"></i><ul id="mapsvg-menu-regions"></ul></div>';
    }elseif($type == 'initiative'){
        $initiatives = array('a'=>'Implementación Plan Prioridades Federativas','b'=>'África y Madagascar','c'=>'Reflexión Educación Popular','d'=>'Comunicación','e'=>'Medición e investigación de impactos','f'=>'Red de Jóvenes','g'=>'Ciudadanía','h'=>'Formación para el Trabajo','i'=>'Innovación Educativa','j'=>'Sistema Mejora de Calidad','k'=>'Género','l'=>'Gestión para Sostenibilidad','m'=>'Educación Especial / Inclusión','n'=>'Fe y Alegría Estados Unidos','o'=>'Relación Organismos Internacionales','p'=>'Formación Pedagógica','q'=>'Panamazonía');
        $html = '<div class="select-map"><a class="call-to" data-menu="mapsvg-menu-initiative">Iniciativas de Trabajo Internacional.</a><i class="fa fa-sort-down"></i><ul id="mapsvg-menu-initiative" class="map-initiative">';
        foreach ($initiatives as $key => $label) {
            $tempL = $key++;
            $html .= '<li data-item="'.$key.'"><a>'.$label.'</a></li>';
        }
        
        $html .='</ul></div>';
    }   
    $html .= '<script type="text/javascript">
    var initiatives = {
    init : ["ar","bo","br","cg","cl","co","do","ec","es","gt","hn","ht","it","mg","ni","pa","pe","py","sv","td","uy","ve","us"],
    init_a : ["ar","bo","br","cg","cl","co","do","ec","es","gt","hn","ht","it","mg","ni","pa","pe","py","sv","td","uy","ve"],
    init_b : ["td","cg","es","mg"],
    init_c : ["ar","bo","br","cg","cl","co","do","ec","es","gt","hn","ht","it","mg","ni","pa","pe","py","sv","td","uy","ve"],
    init_d : ["ar","bo","br","cg","cl","co","do","ec","es","gt","hn","ht","it","mg","ni","pa","pe","py","sv","td","uy","ve"],
    init_e : ["ar","bo","co","sv","gt","ni","uy","ve"],
    init_f : ["ar","bo","br","cg","cl","co","do","ec","es","gt","hn","ht","ni","pa","pe","py","sv","uy","ve"],
    init_g : ["ar","co","sv","es","gt","hn","it","ni","py","do","ve"],
    init_h : ["ar","br","td","cl","co","cg","ec","sv","it","ni","py","pe","uy","ve"],
    init_i : ["bo","cl","co","cg","ec","gt","ht","it","ni","pe","uy","ve"],
    init_j: ["bo","td","sv","gt","ht","hn","ni","pa","do","uy","ve"],
    init_k: ["ar","bo","es","ht","ni","pe","do"],
    init_l: ["ar","bo","br","cg","cl","co","do","ec","es","gt","hn","ht","it","mg","ni","pa","pe","py","sv","td","uy","ve","us"],
    init_m : ["ar","gt","do"],
    init_n: ["ar","bo","br","cg","cl","co","do","ec","es","gt","hn","ht","it","mg","ni","pa","pe","py","sv","td","uy","ve","us"],
    init_o: [],
    init_p: ["ar","bo","br","cg","do","ec","gt","ht","it","ni","pa","pe","sv","td","uy","ve","us"],
    init_q: ["br","co","sv","pe","ve"]
    }
    jQuery(".select-map .call-to").click(function(){ 
        var datamenu = jQuery(this).attr("data-menu"); 
        jQuery(".select-map ul").hide();
        jQuery("#"+datamenu).css("display","block"); 
    });
    jQuery("#mapsvg-menu-regions li, #mapsvg-menu-initiative li").click(function(){ 
        jQuery("#mapsvg path").css("fill","rgb(178,178,178)");
        jQuery(this).parent("ul").css("display","none"); 
        var idiniti = jQuery(this).attr("data-item"); 
        if(idiniti){ 
            jQuery.each(initiatives["init_"+idiniti], function(index,item){
                jQuery("path#"+item).css("fill","rgb(237, 181, 6)");
            });
        }else{
            jQuery.each(initiatives["init"], function(index,item){
                jQuery("path#"+item).css("fill", "rgb(210, 10, 17)");
            });
        }
    });
    jQuery("#mapsvg-menu-regions, #mapsvg-menu-initiative").on("mouseleave",function() {
        console.log("a");
        jQuery(this).css("display","none");
    });
    $("body").on("touchstart", function(e){ 
        jQuery("#mapsvg-menu-regions, #mapsvg-menu-initiative").css("display","none");   
    });
</script>';
    return $html;
}
function geolocation() {
    if (is_main_site()) {
        $urlGEO = 'https://www.feyalegria.org/geolocation.php';
        $dataGEO = file_get_contents($urlGEO);
        $geocountry = json_decode($dataGEO);
        $countrycode = $geocountry->country->code;
        $countryname = $geocountry->country->name;
        if($countrycode != 'INT'){
            if( is_front_page()){
                wp_redirect( 'https://www.feyalegria.org/'.$countryname, 301 ); 
                exit;
            }
        }
    }
}
//add_action( 'wp', 'geolocation' );