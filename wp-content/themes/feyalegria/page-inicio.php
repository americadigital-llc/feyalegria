<?php get_header(); ?>

<!--start section-->
<div class="nicdark_section nicdark_border_bottom_1_solid_grey">
    <!--start nicdark_container-->
    <div class="nicdark_container nicdark_clearfix" id="mainMap">
        <div class="nicdark_grid_12">
        	<div class="nicdark_grid_1"></div>
        	<div class="nicdark_grid_10">
            	<div id="mapsvg"></div>
        	</div>
        	<div class="nicdark_grid_1"></div>
<script type="text/javascript">
jQuery(document).ready(function(){
jQuery("#mapsvg").mapSvg({width: 1366,height: 768,zoom: {on: true},mouseOver: function(e, mapsvg) { var idReg = this.id; var imgregion = "#img-"+idReg; jQuery("#"+idReg).css("fill","url("+imgregion+")"); mapsvg.zoom(null, 2);},onClick : function(event,methods){
   methods.zoom(null, 2);}, colors: {baseDefault: "#000000",background: "#ffffff",selected: 30,hover: 10,directory: "#fafafa",status: {},base: "d20a11",disabled: "#ffb1b1"},regions: {td: {id: "td",'id_no_spaces': "td",tooltip: "Chad",popover: "<h1>República del Chad</h1><p>Fundación: 2007<br />Participantes: 12.012<br />Colaboradores: 278<br />Puntos geográficos: 40</p>",data: {}},ar: {id: "ar",'id_no_spaces': "ar",tooltip: "Argentina",popover: "<h1>Argentina</h1><p>Fundación: 1996<br />Participantes: 6.418<br />Colaboradores: 490<br />Puntos geográficos: 26</p>",data: {}},bo: {id: "bo",'id_no_spaces': "bo",tooltip: "Bolivia",popover: "<h1>Bolivia</h1><p>Fundación: 1966<br />Participantes: 384.802<br />Colaboradores: 10.034<br />Puntos geográficos: 407</p>",data: {}},br: {id: "br",'id_no_spaces': "br",tooltip: "Brasil",popover: "<h1>Brasil</h1><p>Fundación: 1981<br />Participantes: 9.812<br />Colaboradores: 543<br />Puntos geográficos: 25</p>",data: {}},cg: {id: "cg",'id_no_spaces': "cg",tooltip: "República Democrática del Congo",popover: "<h1>República Democrática del Congo</h1><p>Fundación: 2014<br />Participantes: 10.885<br />Colaboradores: 405<br />Puntos geográficos: 6</p>",data: {}},cl: {id: "cl",'id_no_spaces': "cl",tooltip: "Chile",popover: "<h1>Chile</h1><p>Fundación: 2005<br />Participantes: 3.969<br />Colaboradores: 380<br />Puntos geográficos: 13</p>",data: {}},co: {id: "co",'id_no_spaces': "co",tooltip: "Colombia",popover: "<h1>Colombia</h1><p>Fundación: 1971<br />Participantes: 154.444<br />Colaboradores: 2.966<br />Puntos geográficos: 101</p>",data: {}},do: {id: "do",'id_no_spaces': "do",tooltip: "Republica Dominicana",popover: "<h1>Republica Dominicana</h1><p>Fundación: 1990<br />Participantes: 103.267<br />Colaboradores: 2.529<br />Puntos geográficos: 63</p>",data: {}},ec: {id: "ec",'id_no_spaces': "ec",tooltip: "Ecuador",popover: "<h1>Ecuador</h1><p>Fundación: 1964<br />Participantes: 30.402<br />Colaboradores: 1.589<br />Puntos geográficos: 73</p>",data: {}},es: {id: "es",'id_no_spaces': "es",tooltip: "España",popover: "<h1>España</h1><p>Fundación: 1985<br />Participantes: 4.681<br />Colaboradores: 418<br />Puntos geográficos: 13</p>",data: {}},gt: {id: "gt",'id_no_spaces': "gt",tooltip: "Guatemala",popover: "<h1>Guatemala</h1><p>Fundación: 1976<br />Participantes: 31.688<br />Colaboradores: 880<br />Puntos geográficos: 51</p>",data: {}},hn: {id: "hn",'id_no_spaces': "hn",tooltip: "Honduras",popover: "<h1>Honduras</h1><p>Fundación: 2000<br />Participantes: 10.165<br />Colaboradores: 318<br />Puntos geográficos: 34</p>",data: {}},ht: {id: "ht",'id_no_spaces': "ht",tooltip: "Haití",popover: "<h1>Haití</h1><p>Fundación: 2006<br />Participantes: 4.940<br />Colaboradores: 319<br />Puntos geográficos: 19</p>",data: {}},it: {id: "it",'id_no_spaces': "it",tooltip: "Italia",popover: "<h1>Italia</h1><p>Fundación: 2001<br />Participantes: 139<br />Colaboradores: 44<br />Puntos geográficos: 3</p>",data: {}},mg: {id: "mg",'id_no_spaces': "mg",tooltip: "Madagascar",popover: "<h1>Madagascar</h1><p>Fundación: 2013<br />Participantes: 4.750<br />Colaboradores: 130<br />Puntos geográficos: 42</p>",data: {}},ni: {id: "ni",'id_no_spaces': "ni",tooltip: "Nicaragua",popover: "<h1>Nicaragua</h1><p>Fundación: 1974<br />Participantes: 55.578<br />Colaboradores: 854<br />Puntos geográficos: 47</p>",data: {}},pa: {id: "pa",'id_no_spaces': "pa",tooltip: "Panamá",popover: "<h1>Panamá</h1><p>Fundación: 1965<br />Participantes: 2.425<br />Colaboradores: 10<br />Puntos geográficos: 4</p>",data: {}},pe: {id: "pe",'id_no_spaces': "pe",tooltip: "Perú",popover: "<h1>Perú</h1><p>Fundación: 1966<br />Participantes: 132.799<br />Colaboradores: 132.799<br />Puntos geográficos: 81</p>",data: {}},py: {id: "py",'id_no_spaces': "py",tooltip: "Paraguay",popover: "<h1>Paraguay</h1><p>Fundación: 1992<br />Participantes: 11.795<br />Colaboradores: 669<br />Puntos geográficos: 68</p>",data: {}},sv: {id: "sv",'id_no_spaces': "sv",tooltip: "El Salvador",popover: "<h1>El Salvador</h1><p>Fundación: 1969<br />Participantes: 20.335<br />Colaboradores: 482<br />Puntos geográficos: 22</p>",data: {}},us: {id: "us",'id_no_spaces': "us",tooltip: "Estados Unidos",popover: "<h1>Estados Unidos</h1><p>Partidarios, colaboradores y donantes ayudan en la misión.</p>",data: {}},uy: {id: "uy",'id_no_spaces': "uy",tooltip: "Uruguay",popover: "<h1>Uruguay</h1><p>Fundación: 2008<br />Participantes: 2.033<br />Colaboradores: 214<br />Puntos geográficos: 27</p>",data: {}},ve: {id: "ve",'id_no_spaces': "ve",tooltip: "Venezuela",popover: "<h1>Venezuela</h1><p>Fundación: 1955<br />Participantes: 341.196<br />Colaboradores: 11.927<br />Puntos geográficos: 448</p>",data: {}}},viewBox: [0,0,1366,768],cursor: "pointer",tooltips: {mode: "title",on: false,priority: "local",position: "bottom-right"},popovers: {mode: "off",on: false,priority: "local",position: "top",centerOn: false,width: 300,maxWidth: 50,maxHeight: 50,resetViewboxOnClose: true,mobileFullscreen: true},gauge: {on: false,labels: {low: "low",high: "high"},colors: {lowRGB: {r: 85,g: 0,b: 0,a: 1},highRGB: {r: 238,g: 0,b: 0,a: 1},low: "#550000",high: "#ee0000",diffRGB: {r: 153,g: 0,b: 0,a: 0}},min: 0,max: false},menu: {on: true,containerId: "mapsvg-menu-regions",template: function(region){ 
    return '<li><a href="#' + region.id + '" onclick="jQuery(\'#mapsvg-menu-regions\').hide()">' + (region.title||region.id) + '</a></li>'
}},source: "<?php echo get_template_directory_uri(); ?>/assets/img/mapFeAlegria.svg",title: "MapFeAlegria",responsive: true});
});
</script>
		</div>
		<div class="nicdark_grid_12">
			<div class="nicdark_grid_1"></div>
			<div class="nicdark_grid_10">
				<div class="nicdark_grid_4">
					<h1>#SomosFeyAlegria</h1>
				</div>
				<div class="nicdark_grid_4">
					<div class="select-map">
						<a class="call-to" data-menu="mapsvg-menu-regions">Pais </a>
						<i class="fa fa-sort-down"></i>
						<ul id="mapsvg-menu-regions"></ul>
					</div>
					<script type="text/javascript">
						jQuery('.select-map .call-to').click(function(){
							var datamenu = jQuery(this).attr('data-menu');
							jQuery('#'+datamenu).css('display','block');
						});
						
					</script>
				</div>
				<div class="nicdark_grid_4">
				
				
				</div>
			</div>
			<div class="nicdark_grid_1"></div>
		</div>
            <div class="nicdark_section nicdark_height_80"></div>
        
    </div>
    <!--end container-->
</div>
<!--end section-->

	
	
<!--START section-->
<div class="nicdark_section nicdark_border_bottom_1_solid_grey">
    <!--start container-->
    <div class="nicdark_container nicdark_clearfix">
        <div class="nicdark_grid_12">
			<?php
			$categories = get_categories( array(
			    'exclude'   => '14,47', 'orderby' => 'id',
			) );
			?>
			<?php foreach ($categories as $cat) : ?>
			
				<div class="nicdark_clearfix nicdark_grid_4_inicio">
					<div class="li_categoria color_categoria" style="background-image: url('<?php echo z_taxonomy_image_url($cat->term_id); ?>');">
				
						<a href="<?php echo get_category_link($cat->term_id); ?>" title="<?php echo $cat->cat_name; ?>" class="texto_categoria">
							<div class="img_overlay img_overlay_color"><?php echo $cat->cat_name; ?></div>
						</a>
						<img src="<?php bloginfo('stylesheet_directory');?>/img/vc_gitem_image.png" class="img_categoria" alt="<?php echo $cat->cat_name; ?>">
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
	<!--end container-->
</div>
<!--END section-->
	
<div class="nicdark_section nicdark_height_30"></div>

<!--start section-->
<div class="nicdark_section bg-contacto">
    <!--start nicdark_container-->
    <div class="nicdark_container nicdark_clearfix">
        <div class="nicdark_grid_12">
            <div class="nicdark_section nicdark_height_20"></div>
				<?php echo do_shortcode( '[contact-form-7 id="1156" title="Contacto Principal"]' ); ?>
            <div class="nicdark_section nicdark_height_20"></div>
        </div>
    </div>
    <!--end container-->
</div>
<!--end section-->

<div class="nicdark_section nicdark_height_30"></div>

<!--start section-->
<div class="nicdark_section">
    <!--start nicdark_container-->
    <div class="nicdark_container nicdark_clearfix">
		<div class="nicdark_grid_8 nicdark_text_align_center_responsive padding-0">
			<div class="vc_row margin-0">
				<div class="vc_column_container nicdark_grid_6  padding-0">
					<div class="vc_column-inner padding-0 video-home">
						<?php query_posts('cat=47&showposts=1'); ?>
						<?php while(have_posts()) : the_post();  $post_id = get_the_ID(); ?>
							<?php
								//info
								$nd_options_id = get_the_ID(); 
								$nd_options_title = get_the_title();
								$nd_options_excerpt = get_the_excerpt();
								$nd_options_permalink = get_permalink( $nd_options_id );
								$embed = get_post_meta( $post_id, 'embed', true ); 

								//image
								$nd_options_image_id = get_post_thumbnail_id( $nd_options_id );
								$nd_options_image_attributes = wp_get_attachment_url(get_post_thumbnail_id());
								if ( $nd_options_image_attributes[0] == '' ) { $nd_options_output_image = ''; }else{
								  $nd_options_output_image = '<img class="nd_options_section" alt="" src="'.$nd_options_image_attributes[0].'">';
								}

								//metabox
								$nd_options_meta_box_page_color = get_post_meta( $nd_options_id, 'nd_options_meta_box_post_color', true );
								if ( $nd_options_meta_box_page_color == '' ) { $nd_options_meta_box_page_color = '#000'; }
							?>
							<?php
								//display
								$nd_options_customizer_archives_archive_image_display = get_option( 'nd_options_customizer_archives_archive_image_display' );
							?>
							<?php

								//header image
								$nd_options_customizer_archives_archive_image = get_option( 'nd_options_customizer_archives_archive_image' );
								if ( $nd_options_customizer_archives_archive_image == '' ) { 
									$nd_options_customizer_archives_archive_image = '';  
								}else{
									$nd_options_customizer_archives_archive_image = wp_get_attachment_url(get_post_thumbnail_id());
								}

								//position
								$nd_options_customizer_archives_archive_image_position = get_option( 'nd_options_customizer_archives_archive_image_position' );
								if ( $nd_options_customizer_archives_archive_image_position == '' ) { 
									$nd_options_customizer_archives_archive_image_position = 'nd_options_background_position_center_top';  
								}
							?>
						
				<?php echo do_shortcode( '[nd_options_magic_popup]' ); ?>

							<div class="vc_column-inner">
								<div class="wpb_wrapper">
									<div class="vc_row wpb_row vc_inner vc_row-fluid">
										<div class="nd_options_text_align_center wpb_column vc_column_container vc_col-sm-12 vc_col-has-fill">
											<div class="vc_column-inner" style="background-image: url(<?php echo $nd_options_customizer_archives_archive_image; ?>) !important; background-position: center !important; background-repeat: no-repeat !important; background-size: cover !important;">
												<a class=" nd_options_outline_0 nd_options_mpopup_iframe" href="<?php echo $embed; ?>">
													<img alt="" class="nd_options_transition_all_08_ease nd_options_opacity_05_hover" src="<?php bloginfo('stylesheet_directory');?>/img/icon-play-n.png" style="width: 100%">
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						
						<?php endwhile; wp_reset_query();?>
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="nicdark_grid_4 nicdark_text_align_center_responsive padding-0">
			<?php query_posts('cat=89&showposts=1'); ?>
				<?php if(have_posts()) :
					while(have_posts()) : the_post(); ?>
					<!--#post-->
					<div class="nicdark_section nicdark_container_page_php">
						<!--start content-->
						<?php the_content(); ?>
						<!--end content-->
					</div>
					<!--#post-->
				<?php endwhile; ?>
			<?php endif; wp_reset_query();?>
		</div>

    </div>
    <!--end container-->
</div>
<!--end section-->


<!--start nicdark_container-->
<div class="nicdark_container nicdark_clearfix">
	<div class="nicdark_grid_12 padding-0">
		<?php query_posts('category_name=categoria-2&showposts=1'); ?>
			<?php if(have_posts()) :
				while(have_posts()) : the_post(); ?>
					<!--post-->
					<div class="nicdark_section nicdark_container_page_php" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<!--start content-->
						<?php the_content(); ?>
						<!--end content-->
					</div>
					<!--#post-->
			<?php endwhile; ?>
		<?php endif; wp_reset_query();?>
	</div>
</div>
<!--end container-->


<div class="nicdark_section nicdark_height_60"></div> 
 
<?php get_footer(); ?>