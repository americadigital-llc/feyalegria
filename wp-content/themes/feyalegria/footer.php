<!--START section-->
<div class="nicdark_section nicdark_bg_grey_3 footer-site">

    <!--start container-->
    <div class="nicdark_container nicdark_clearfix">
        

        <div class="nicdark_grid_7 nicdark_text_align_center_responsive menu-pre-footer">

            <div class="nicdark_section nicdark_height_10"></div>

            <div class="nicdark_navigation_2_sidebar">      
                <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>       
            </div>

            <div class="nicdark_section nicdark_height_10"></div>

        </div>
        
        
        <!--START LOGO OR TAGLINE-->
        <?php $nicdark_customizer_logo_img = get_option( 'nicdark_customizer_logo_img' ); ?>
                
        <div class="nicdark_grid_5 nicdark_text_align_center_responsive">

            <div class="nicdark_grid_9 nicdark_text_align_center_responsive pre_footer">

                <?php if ( is_active_sidebar( 'nicdark_footerinfo' ) ) : ?>
                    <?php dynamic_sidebar( 'nicdark_footerinfo' ); ?>
                <?php endif; ?>
                
                <div class="redes-footer"><a href="https://www.facebook.com/FeyAlegriaFI" target="blank" title="facebook"><i class="fa fa-facebook"></i></a> <a href="https://twitter.com/feyalegriafi" target="blank" title="twitter"><i class="fa fa-twitter"></i></a> <a href="https://www.instagram.com/feyalegriafi/" target="blank" title="instagram"><i class="fa fa-instagram"></i></a>
                <a href="https://www.youtube.com/user/feyalegriaFI" target="blank" title="youtube"><i class="fa fa-youtube-play"></i></a></div>

            </div>

            <?php $nicdark_customizer_logo_img = wp_get_attachment_url($nicdark_customizer_logo_img); ?>

            <div class="nicdark_grid_3 nicdark_text_align_center_responsive logo-pre-footer">
                <a href="<?php echo esc_url(home_url()); ?>">
                    <img class="nicdark_section" src="<?php echo esc_url($nicdark_customizer_logo_img); ?>">
                </a>
            </div>              

        </div>
        <!--END LOGO OR TAGLINE-->

    </div>
    <!--end container-->

</div>
<!--END section-->


<!--START section-->
<div class="nicdark_section nicdark_bg_greydark2 nicdark_text_align_center">
    
    <!--start container-->
    <div class="nicdark_container nicdark_clearfix">

        <div class="nicdark_grid_12">

            <div class="nicdark_section nicdark_height_10"></div>

            <p class="contact_footer">
                <?php if ( is_active_sidebar( 'nicdark_siteinfo' ) ) : ?>
                    <?php dynamic_sidebar( 'nicdark_siteinfo' ); ?>
                <?php endif; ?>
            </p>
            
            <div class="nicdark_section nicdark_height_10"></div>

        </div>

    </div>
    <!--end container-->

</div>
<!--END section-->



<!--START section-->
<div class="nicdark_section nicdark_bg_greydark2 nicdark_text_align_center">
    
    <!--start container-->
    <div class="nicdark_container nicdark_clearfix">
                
        <div class="nicdark_grid_4 nicdark_text_align_center_responsive">

            &copy;<?php echo date('Y');?>  Todos los derechos reservados

        </div>
        
        
        <div class="nicdark_grid_8 nicdark_text_align_center_responsive">

            <div class="nicdark_section nicdark_navigation_1">        
                <?php wp_nav_menu( array( 'theme_location' => 'footer2-menu' ) ); ?>       
            </div>

        </div>



    </div>
    <!--end container-->

</div>
<!--END section-->



</div>
<!--END theme-->

  <script type="text/javascript">
    //<![CDATA[
    
    jQuery(document).ready(function() {

      //START counter
      jQuery(function ($) {
        
        $("#nd_options_autocomplete_search").on("click", function () { 
          $( "#nd_options_site_filter" ).addClass("nd_options_active");
          $( "#nd_options_autocomplete_search_result" ).css("display","block");
        });

        $( "#nd_options_site_filter" ).on( "click", function() {
          $( "#nd_options_site_filter" ).removeClass("nd_options_active");
          $( "#nd_options_autocomplete_search_result" ).css("display","none");
        });
        
        $("#nd_options_autocomplete_search").on("input",function(e){
            var nd_options_keyword = $( this ).val();
            var nd_options_category_slug = $("#nd_options_autocomplete_search_category_slug").val();
            nd_options_get_ajax_search_results(nd_options_keyword,nd_options_category_slug);
        });

      });
      //END counter
    });

    //]]&gt;
  </script>


<?php wp_footer(); ?>

    
</body>  
</html>