<?php
$country_code = $_SERVER["HTTP_CF_IPCOUNTRY"];
//$country_code = $_SERVER['HTTP_CLOUDFRONT_VIEWER_COUNTRY'];
//$country_code = 'VE';
switch ($country_code) {
	case 'IT':
        $name = 'italia';
        break;
    case 'VE':
        $name = 'venezuela';
        break;
    case 'CL':
        $name = 'chile';
        break;
    case 'UY':
        $name = 'uruguay';
        break;
    default:
    	$name = 'internacional';
    	$country_code = 'INT';
}
$data = array('country'=> array(
        'name' => $name,
        'code' => $country_code
        )
);
header('Content-Type: application/json');
echo json_encode($data);
?>